class Location {
    constructor() {
        this.mapsApiKey = '[GOOGLE-MAPS-API-KEY]';
        this.restaurantURI = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
        this.distanceURI = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric';
        this.speedInKmHr = 5;
        this.proxyURL = `https://still-gorge-38865.herokuapp.com/`;
    }

    async restaurantList(latitude, longitude) {
        const query = `?location=${latitude},${longitude}&type=restaurant&rankby=distance&key=${this.mapsApiKey}`;
        const response = await fetch(this.proxyURL + this.restaurantURI + query);
        return response.json();
    }

    async calcDrivableDistance(homeCord, nearCord, farCord) {//For calculating car driving distance
        const query = `&origins=${homeCord.lat},${homeCord.lng}|${nearCord.lat},${nearCord.lng}
        &destinations=${nearCord.lat},${nearCord.lng}|${farCord.lat},${farCord.lng}&key=${this.mapsApiKey}`;
        const response = await fetch(this.proxyURL + this.distanceURI + query);
        return response.json();
    }


    calcSides(homeCord, nearCord, farCord) {
        const a = this.haversine_distance(homeCord, nearCord);
        const b = this.haversine_distance(homeCord, farCord);
        const c = this.haversine_distance(nearCord, farCord);
        return this.calcAreaAndTime(a, b, c);
    }

    haversine_distance(mk1, mk2) {
        var R = 6371.0710; // Radius of the Earth in Km
        var rlat1 = mk1.lat * (Math.PI / 180); // Convert degrees to radians
        var rlat2 = mk2.lat * (Math.PI / 180); // Convert degrees to radians
        var difflat = rlat2 - rlat1; // Radian difference (latitudes)
        var difflon = (mk2.lng - mk1.lng) * (Math.PI / 180); // Radian difference (longitudes)

        var d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat / 2) * Math.sin(difflat / 2) + Math.cos(rlat1) * Math.cos(rlat2) * Math.sin(difflon / 2) * Math.sin(difflon / 2)));
        return d;
    }

    calcAreaAndTime(a, b, c) {
        const s = (a + b + c) / 2;
        const product = s * (s - a) * (s - b) * (s - c);
        const areaInKm = Math.sqrt(product);
        const timeInHr = (2 * s) / this.speedInKmHr;
        return { areaInKm, timeInHr };
    }
}

export default Location;


