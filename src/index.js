import './css/styles.css';
import Location from './restaurant-triangle/Location';

const location = new Location();
const confirmBtn = document.getElementById('confirmPosition');
const onClickPositionView = document.getElementById('onClickPositionView');
const onIdlePositionView = document.getElementById('onIdlePositionView');
const task1 = document.querySelector('#task1');
const task2 = document.querySelector('#task2');

let latitude,longitude;

const loadMap = function () {

    var script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=${location.mapsApiKey}&callback=initMap`;
    script.defer = true;

    window.initMap = function () {
        var lp = new locationPicker('map', {
            setCurrentPosition: true,
        }, {
            zoom: 15
        });  
        google.maps.event.addListener(lp.map, 'idle', function (event) {
            var location = lp.getMarkerPosition();
            latitude = location.lat;
            longitude = location.lng;
            onIdlePositionView.innerHTML = location.lat + ',' + location.lng;
        });
    }
    document.head.appendChild(script);

};

loadMap();

confirmBtn.onclick = function () {
    task1.innerHTML = 'Loading...';
    task2.innerHTML = 'Loading...';
    onClickPositionView.innerHTML = `The selected coordinates are ${latitude}, ${longitude}`;
    const homeCord = { lat: latitude, lng: longitude };
    location.restaurantList(latitude, longitude).then(response => {
        const nearestRestaurantCord = { lat: response.results[0].geometry.location.lat, lng: response.results[0].geometry.location.lng };
        const farthestRestaurantCord = { lat: response.results[9].geometry.location.lat, lng: response.results[9].geometry.location.lng };
        const task1Ans = location.calcSides(homeCord, nearestRestaurantCord, farthestRestaurantCord);
        const places = { nearestPlace: response.results[0].name, farthestPlace: response.results[9].name };
        task1.innerHTML = `<p>Nearest restaurant: ${places.nearestPlace}</p><p>Farthest restaurant: ${places.farthestPlace}</p>`
        task1.innerHTML += `Area of triangle whose edges are straight lines in Km<sup>2</sup> is: <b>${Number(task1Ans.areaInKm.toFixed(3))}</b> and time to walk the triangle at ${location.speedInKmHr}km/hr in hr: <b>${Number(task1Ans.timeInHr.toFixed(3))}</b>.`;
        return location.calcDrivableDistance(homeCord, nearestRestaurantCord, farthestRestaurantCord);
    }).then(response => {
        const homeToNearRest = Number((response.rows[0].elements[0].distance.value / 1000).toFixed(1));
        const homeToFarRest = Number((response.rows[0].elements[1].distance.value / 1000).toFixed(1));
        const nearToFarRest = Number((response.rows[1].elements[1].distance.value / 1000).toFixed(1));
        const task2Ans = location.calcAreaAndTime(homeToNearRest, homeToFarRest, nearToFarRest);
        const { areaInKm, timeInHr } = task2Ans;
        task2.innerHTML = !isNaN(areaInKm) ?`<p>Area of triangle whose sides equal car driving distance in Km<sup>2</sup> is 
        <b>${Number(areaInKm.toFixed(3))}</b> and time taken to walk the triangle at ${location.speedInKmHr}km/hr
         in hr: <b>${Number(timeInHr.toFixed(3))}</b>.</p>` : `<p>Area of triangle whose sides equal car driving distance in Km<sup>2</sup> is 
         <b class = "error small">CANNOT BE CALCULATED</b> and time taken to walk the triangle at ${location.speedInKmHr}km/hr
          in hr: <b>${Number(timeInHr.toFixed(3))}</b>.</p>`;//Value of area can be NaN if triangle formed is not proper          
    }).catch(err => {
        console.log(err);
    })

};